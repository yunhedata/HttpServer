package org.qtk.server;

import java.util.HashMap;
import java.util.Map;

/**
 * 上下文 一个容器
 * 
 * @author SiriusTK
 * 
 */
public class ServletContext {

	private Map<String, String> servlet; // 为每一个servlet起一个别名 如:login -->
											// LoginServlet
	private Map<String, String> mapping; // url映射 如 /log-->login /login-->login

	public ServletContext() {
		servlet = new HashMap<String, String>();
		mapping = new HashMap<String, String>();
	}

	public Map<String, String> getServlet() {
		return servlet;
	}

	public void setServlet(Map<String, String> servlet) {
		this.servlet = servlet;
	}

	public Map<String, String> getMapping() {
		return mapping;
	}

	public void setMapping(Map<String, String> mapping) {
		this.mapping = mapping;
	}

}
