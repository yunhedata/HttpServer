package org.qtk.server;

/**
 * <servlet> <servlet-name>login</servlet-name>
 * <servlet-class>org.qtk.server.LoginServlet</servlet-class> </servlet>
 * 
 * @author SiriusTK
 * 
 */
public class Entity {
	private String name;
	private String clz;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClz() {
		return clz;
	}

	public void setClz(String clz) {
		this.clz = clz;
	}

}
