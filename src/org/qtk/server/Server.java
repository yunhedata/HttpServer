package org.qtk.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	private ServerSocket serverSocket;
	private boolean isShutdown = false;

	public static void main(String[] args) {
		Server server = new Server();
		server.start();
	}

	public void start() {
		try {
			serverSocket = new ServerSocket(8888);
			receive();
		} catch (IOException e) {
			e.printStackTrace();
			stop();
		}
	}

	private void receive() {
		try {
			while (!isShutdown) {
				new Thread(new Dispatcher(serverSocket.accept())).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void stop() {
		isShutdown = true;
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
