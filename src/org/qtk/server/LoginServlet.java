package org.qtk.server;

public class LoginServlet extends Servlet {

	@Override
	public void doGet(Request request, Response response) throws Exception {
		doPost(request, response);
	}

	@Override
	public void doPost(Request request, Response response) throws Exception {
		response.print("<html>" +
				"<head>" +
				"<title>" +
				"http响应页面" +
				"</title>" +
				"</head>" +
				"<body>" +
				"欢迎： " + request.getParameter("username")+" 光临"+
				"</body>" +
				"</html>");
		response.pushToClient(200);
	}

}
