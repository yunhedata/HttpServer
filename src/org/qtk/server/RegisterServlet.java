package org.qtk.server;

public class RegisterServlet extends Servlet {

	@Override
	public void doGet(Request request, Response response) throws Exception {
		doPost(request, response);
	}

	@Override
	public void doPost(Request request, Response response) throws Exception {
		response.print("<html>" +
				"<head>" +
				"<title>" +
				"注册成功" +
				"</title>" +
				"</head>" +
				"<body>" +
				"注册成功 你的用户名为： " + request.getParameter("username")+
				"</body>" +
				"</html>");
		response.pushToClient(200);
	}

}
