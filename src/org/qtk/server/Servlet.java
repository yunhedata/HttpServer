package org.qtk.server;

public abstract class Servlet {

	protected void service(Request request, Response response) throws Exception {
		if (request.getMethod().equalsIgnoreCase("POST")) {
			doPost(request, response);
		} else {
			doGet(request, response);
		}
	}

	public abstract void doGet(Request request, Response response)
			throws Exception;

	public abstract void doPost(Request request, Response response)
			throws Exception;
}
